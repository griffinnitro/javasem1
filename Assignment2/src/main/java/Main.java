import dal.Dao;
import utils.HibernateSessionFactoryUtil;

public class Main {
    public static void main(String[] args) {
        Dao dao = new Dao();
        System.out.println("------------getTeacher(2)--------------");
        System.out.println(dao.getTeacher(2l));
        System.out.println("------------getTeachers()--------------");
        System.out.println(dao.getTeachers());
        System.out.println("------------getStudent(4)--------------");
        System.out.println(dao.getStudent(4l));
        System.out.println("------------getStudents()--------------");
        System.out.println(dao.getStudents());
        System.out.println("------------getGroup(1)--------------");
        System.out.println(dao.getGroup(1l));
        System.out.println("------------getGroups()--------------");
        System.out.println(dao.getGroups());

        // ending work with hibernate
        HibernateSessionFactoryUtil.getSessionFactory().close();
    }
}
