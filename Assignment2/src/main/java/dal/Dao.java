package dal;

import dal.implementations.GenericDaoImpl;
import entity.*;

import javax.persistence.Temporal;
import java.util.List;

public class Dao {
    GenericDaoImpl impl;

    public Teacher getTeacher(Long id) {
        impl = new GenericDaoImpl(Teacher.class);
        return (Teacher) impl.getById(id);
    }

    public Student getStudent(Long id) {
        impl = new GenericDaoImpl(Student.class);
        return (Student) impl.getById(id);
    }

    public Group getGroup(Long id) {
        impl = new GenericDaoImpl(Group.class);
        return (Group) impl.getById(id);
    }

    public List<Teacher> getTeachers () {
        impl = new GenericDaoImpl(Teacher.class);
        return impl.getAll();
    }

    public List<Student> getStudents () {
        impl = new GenericDaoImpl(Student.class);
        return impl.getAll();
    }

    public List<Group> getGroups () {
        impl = new GenericDaoImpl(Group.class);
        return impl.getAll();
    }

}
