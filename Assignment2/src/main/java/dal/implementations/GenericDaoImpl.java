package dal.implementations;

import dal.interfaces.GenericDao;
import utils.HibernateSessionFactoryUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GenericDaoImpl<T, PK extends Serializable> implements GenericDao<T, PK> {
    private Class<T> type;

    public GenericDaoImpl(Class<T> type) {
        this.type = type;
    }

    @Override
    public T getById(PK id) {
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(type, id);
    }

    @Override
    public List<T> getAll() {
        List<T> objects = (List<T>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From " + type.getName()).list();
        return objects;
    }
}
