package dal.interfaces;

import java.io.Serializable;
import java.util.List;

public interface GenericDao <T, PK extends Serializable> {
    //read
    T getById(PK id);

    List<T> getAll();
}
