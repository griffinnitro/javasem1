package entity;

import javax.persistence.*;
import java.sql.Date;

@Entity(name = "students")
public class Student extends Person {

    @Column(name = "personal_number")
    private Integer personalNumber;

    /*@OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private Integer idGroup;*/

    @ManyToOne (optional=false, cascade=CascadeType.ALL)
    @JoinColumn (name="group_id")
    private Group group;

    public Student(Long id, String firstName
            , String middleName
            , String lastName
            , Date dateOfBirth
            , Integer personalNumber
            , Integer idGroup
    ) {
        super(id, firstName, middleName, lastName, dateOfBirth);

        this.personalNumber = personalNumber;
    }

    public Student() {
    }

    public Integer getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(Integer personalNumber) {
        this.personalNumber = personalNumber;
    }

    /*public Long getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(Long idGroup) {
        this.idGroup = idGroup;
    }*/

    @Override
    public String toString() {
        return "Student{" +
                "id=" + getId() +
                ", firstName=" + getFirstName() +
                ", middleName=" + getMiddleName() +
                ", lastName=" + getLastName() +
                ", dateOfBirth=" + getDateOfBirth() +
                ", personalNumber=" + personalNumber +
                ", group_id=" + group.getId() +
                "}\n";
    }
}
