package entity;

import javax.persistence.*;
import java.sql.Date;

@Entity(name = "teachers")
public class Teacher extends Person {

    @Column
    private String graduate;

    @Column
    private String email;

    @Column
    private Integer experience;


    Teacher(Long id, String firstName, String middleName
            , String lastName
            , Date dateOfBirth
            , String graduate
            , String email
            , Integer experience
    ) {
        super(id, firstName, middleName, lastName, dateOfBirth);
        this.graduate = graduate;
        this.email = email;
        this.experience = experience;
    }

    public Teacher() {
        super();
    }

    public String getGraduate() {
        return graduate;
    }

    public void setGraduate(String graduate) {
        this.graduate = graduate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", firstName=" + getFirstName() +
                ", middleName=" + getMiddleName() +
                ", lastName=" + getLastName() +
                ", dateOfBirth=" + getDateOfBirth() +
                ", graduate=" + graduate +
                ", email=" + email +
                ", experience=" + experience +
                "}\n";
    }
}
