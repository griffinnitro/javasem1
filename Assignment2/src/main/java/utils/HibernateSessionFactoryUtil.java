package utils;

import entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.io.File;

public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties());
                sessionFactory = configuration.
                        addPackage("entity").
                        addAnnotatedClass(Teacher.class).
                        addAnnotatedClass(Student.class).
                        addAnnotatedClass(Group.class).
                        configure().
                        buildSessionFactory();

            } catch (Exception e) {
                System.out.println(e);
            }
        }
        return sessionFactory;
    }
}
