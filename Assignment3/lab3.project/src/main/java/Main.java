import Sets.*;
public class Main {
        public static void main(String[] args) {
            PurelyFunctionalSet<Integer> set1 = (x) -> x > 0 && x <= 3;
            PurelyFunctionalSet<Integer> set2 = (x) -> x > 1 && x <= 6;

            PurelyFunctionalSet<Integer> union = FunctionalSet.union(set2, set1);
            for(int i = 0; i <= 10; i++)
                System.out.println("i = " + i + " " + union.contains(i));
            System.out.println(SetOfIntegers.forall(union, (x) -> x > 0 && x <= 6 ));
            System.out.println(SetOfIntegers.exists(union, (x) -> x >= 2 && x <= 4));


            PurelyFunctionalSet<Integer> mapSet = SetOfIntegers.map(union, (x) -> x + 2);
            for(int i = 0; i <= 10; i++)
                System.out.println("i = " + i + " " + mapSet.contains(i));
        }
}
