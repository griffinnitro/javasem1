package Sets;

public class FunctionalSet {
    public static<T>PurelyFunctionalSet<T> empty() {
        return (x) -> x == null;
    }

    public static <T>PurelyFunctionalSet<T> singeltonSet(T val) {
        return (x) -> x == val;
    }

    public static <T> PurelyFunctionalSet<T> union(PurelyFunctionalSet<T> s,
                                                   PurelyFunctionalSet<T> t) {
        return (x) -> s.contains(x) || t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> intersect(PurelyFunctionalSet<T> s,
                                                       PurelyFunctionalSet<T> t) {
        return (x) -> s.contains(x) && t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> diff(PurelyFunctionalSet<T> s,
                                                  PurelyFunctionalSet<T> t) {
        return (x) -> s.contains(x) && !t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> filter(PurelyFunctionalSet<T> s,
                                                    Predicate<T> p) {
        return (x) -> p.test(x) && s.contains(x);
    }
}
