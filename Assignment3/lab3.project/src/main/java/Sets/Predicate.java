package Sets;

public interface Predicate<T> {
    boolean test(T Element);
}
