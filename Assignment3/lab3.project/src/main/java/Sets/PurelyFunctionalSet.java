package Sets;

public interface PurelyFunctionalSet<T> {
    boolean contains(T Element);
}
