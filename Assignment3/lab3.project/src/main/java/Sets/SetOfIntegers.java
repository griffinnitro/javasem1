package Sets;

import java.util.function.Function;

public class SetOfIntegers {
    private static int leftBind = -1000;
    private static int rightBind = 1000;

    public static boolean forall(PurelyFunctionalSet<Integer> s,
                                 Predicate<Integer> p) {
        return bypassCollection(leftBind, s, p);
    }
    private static boolean bypassCollection(int elementInSet,
                                            PurelyFunctionalSet<Integer> set, Predicate<Integer> predicate){
        if (elementInSet >= rightBind) return true;
        if (set.contains(elementInSet) && !predicate.test(elementInSet))
            return false;
        else return bypassCollection(++elementInSet, set, predicate);
    }

    public static boolean exists(PurelyFunctionalSet<Integer> s,
                                 Predicate<Integer> p) {
        return !forall(s, (x) -> !p.test(x));
    }

    public static <R> PurelyFunctionalSet<Integer> map(PurelyFunctionalSet<Integer> s,
                                                       Function<Integer, R> p) {
        return (y) -> exists(s, (x) -> p.apply(x) == y);
    }

}