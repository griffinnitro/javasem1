import org.junit.Before;
import org.junit.Test;

import Sets.*;

import static junit.framework.TestCase.*;

public class Tests {

    PurelyFunctionalSet<Integer> setForTesting;

    @Before
    public void init(){
        setForTesting = (x) -> x > 0 && x < 10;
    }

    @Test
    public void empty_none_EmptySet() {
        // arrange
        // act
        PurelyFunctionalSet<Integer> set = FunctionalSet.empty();
        // assert
        assertTrue(set.contains(null));
    }

    @Test
    public void singeltonSet_OneValue_SetFromOneValue() {
        // arrange
        // act
        PurelyFunctionalSet<Integer> set = FunctionalSet.singeltonSet(5);
        // assert
        assertTrue(set.contains(5));
        assertFalse(set.contains(10));
    }

    @Test
    public void union_TwoDifferentFunctionalSet_NewUnionSet() {
        // arrange
        PurelyFunctionalSet<Integer> s = FunctionalSet.singeltonSet(5);
        PurelyFunctionalSet<Integer> t = FunctionalSet.singeltonSet(10);
        // act
        PurelyFunctionalSet<Integer> set = FunctionalSet.union(s, t);
        // assert
        assertTrue(set.contains(5));
        assertTrue(set.contains(10));
    }

    @Test
    public void intersect_TwoFunctionalSetWithSameValues_NewUnionSet() {
        // arrange
        PurelyFunctionalSet<Integer> s = FunctionalSet.singeltonSet(5);
        // act
        PurelyFunctionalSet<Integer> set = FunctionalSet.intersect(s, setForTesting);
        // assert
        assertTrue(set.contains(5));
        assertFalse(set.contains(6));
    }
    @Test
    public void intersect_TwoFunctionalSetWithoutSameValues_EmptySet() {
        // arrange
        PurelyFunctionalSet<Integer> t = FunctionalSet.singeltonSet(10);
        // act
        PurelyFunctionalSet<Integer> set = FunctionalSet.intersect(setForTesting, t);
        // assert
        assertFalse(set.contains(5));
        assertFalse(set.contains(10));
    }

    @Test
    public void diff_TwoDifferentFunctionalSet_NewSetWithUniqueValuesFromFirstSet() {
        // arrange
        PurelyFunctionalSet<Integer> s = FunctionalSet.singeltonSet(5);
        PurelyFunctionalSet<Integer> t = FunctionalSet.singeltonSet(10);
        // act
        PurelyFunctionalSet<Integer> set = FunctionalSet.diff(s, t);
        // assert
        assertTrue(set.contains(5));
        assertFalse(set.contains(10));
    }
    @Test
    public void diff_TwoDifferentFunctionalSet_EmptySet() {
        // arrange
        PurelyFunctionalSet<Integer> s = FunctionalSet.singeltonSet(5);
        // act
        PurelyFunctionalSet<Integer> set = FunctionalSet.diff(s, s);
        // assert
        assertFalse(set.contains(5));
    }

    @Test
    public void filter_SomeSetWithPredication_NewSet() {
        // arrange
        // act
        PurelyFunctionalSet<Integer> set = FunctionalSet.filter(setForTesting, (x) -> x < 10 && x >= 5);
        // assert
        assertTrue(set.contains(5));
        assertTrue(set.contains(8));
        assertFalse(set.contains(10));
    }


    @Test
    public void forall_SetWithTruePredicationForAllElements_True() {
        // arrange
        // act
        boolean result = SetOfIntegers.forall(setForTesting, (x) -> x > 0 && x < 10);
        // assert
        assertTrue(result);
    }
    @Test
    public void forall_SetWithTruePredicationNotForAllElements_False() {
        // arrange
        // act
        boolean result = SetOfIntegers.forall(setForTesting, (x) -> x > 1 && x <= 10);
        // assert
        assertFalse(result);
    }

    @Test
    public void exists_SetWithPredication_True() {
        // arrange
        // act
        boolean result = SetOfIntegers.exists(setForTesting, (x) -> x > 0 && x < 3);
        // assert
        assertTrue(result);
    }
    @Test
    public void exists_SetWithPredicationForNoElements_False() {
        // arrange
        // act
        boolean result = SetOfIntegers.exists(setForTesting, (x) -> x < 0 && x > -10);
        // assert
        assertFalse(result);
    }

    @Test
    public void map_NewFunctionToSet_NewSet() {
        // arrange
        int[] arr = new int[]{1,4,9,16,25,36,49,64,81};
        // act
        PurelyFunctionalSet<Integer> set = SetOfIntegers.map(setForTesting, (x) -> x * x);
        // assert
        for (int el: arr){
            assertTrue(set.contains(el));
        }
    }

}
